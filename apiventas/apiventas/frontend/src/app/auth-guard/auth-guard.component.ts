import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { Router, CanActivate } from '@angular/router';

@Component({
  selector: 'app-auth-guard',
  templateUrl: './auth-guard.component.html',
  styleUrls: ['./auth-guard.component.css']
})
export class AuthGuardComponent implements CanActivate {

  constructor( private clientesService: ClienteService,
    private router: Router) { }

  ngOnInit(): void {
  }
  canActivate(): boolean {
  debugger;
    if (this.clientesService.loggedIn()) {
      return true;
    }

    this.router.navigate(['/home']);
    return false;
  }

}
