import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ClienteService } from '../services/cliente.service';


@Injectable({
  providedIn: 'root'
})

export class AuthAdmin implements CanActivate {

  role:string;
  constructor(private router: Router, private _service: ClienteService) {
  }
  canActivate(): boolean {

    debugger;
    
    if (this._service.IsAdmin()) {
      return true;
    }

    this.router.navigate(['/home']);
    return false;
  }
}
