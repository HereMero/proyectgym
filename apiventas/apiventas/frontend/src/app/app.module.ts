import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { from } from 'rxjs';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ErrorComponent } from './components/error/error.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import {MatSliderModule} from '@angular/material/slider';
import { ClientestablaComponent } from './components/clientestabla/clientestabla.component';
import { RutinasComponent } from './components/rutinas/rutinas.component';
import { AuthGuardComponent } from './auth-guard/auth-guard.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { FilterClienPipe } from './Pipes/filter-clien.pipe';
import { SelectComponent } from './components/select/select.component';
@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    LoginComponent,
    NavbarComponent,
    ErrorComponent,
    HomeComponent,
    ClientestablaComponent,
    RutinasComponent,
    PerfilComponent,
    FilterClienPipe,
    SelectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSliderModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()

  ],
  providers: [AuthGuardComponent,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
