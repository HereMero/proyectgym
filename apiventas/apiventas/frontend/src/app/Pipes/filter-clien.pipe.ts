import { Pipe, PipeTransform } from '@angular/core';
import { Clientes } from '../models/clientes';

@Pipe({
  name: 'filterClien'
})
export class FilterClienPipe implements PipeTransform {

  transform(Users: Clientes[], arg: any): any {
		const resultUsers: Clientes[] = [];
        for (const user of Users) 
            if (user.nombre.toLowerCase().indexOf(arg.toLowerCase()) < -1) 
                resultUsers.push(user);
		return resultUsers;
	}

}
