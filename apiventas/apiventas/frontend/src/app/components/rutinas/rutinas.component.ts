import { Component, OnInit } from '@angular/core';
import { FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import { RutinasService } from 'src/app/services/rutinas.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { Rutinas } from 'src/app/models/rutinas';
import { ClienteService } from 'src/app/services/cliente.service';
import { Clientes } from 'src/app/models/clientes';
declare var M: any;
@Component({
  selector: 'app-rutinas',
  templateUrl: './rutinas.component.html',
  styleUrls: ['./rutinas.component.css']
})
export class RutinasComponent implements OnInit {

  constructor(public RutinasService: RutinasService,public clienteService: ClienteService, private router: Router, private toastr: ToastrService)  
  { 
   
  }

  ngOnInit(): void {
    this.getRutinas();
    this.resetForm();
  }
  getRutinas() {
    this.RutinasService.getrutina()
      .subscribe(res => {
        this.RutinasService.rutinas1= res as Rutinas[];
      });

      
  }
 
  get(_id: string) {
    this.clienteService.get(_id)
      .subscribe(res => {
        this.clienteService.clientes1 = res as Clientes[];
      });

    }

addrutina(form: NgForm)
{
  debugger
this.RutinasService.postrutina(form.value).subscribe(res =>{console.log(res)});
}

resetForm(form?: NgForm)
  {
   if(form)
   { 
    form.reset();
    this.RutinasService.selectrutina = new Rutinas();
   }
  }
  onEdit(rutina: Rutinas) {
    debugger;
   this.RutinasService.selectrutina = rutina;
 }
 deleteRutina(_id: string, form: NgForm ){
  debugger;
  this.RutinasService.deleterutina(_id).subscribe((data) => {
    
    this.refrescarListaRutina();
      this.resetForm(form);
  M.toast({html: 'Se elimino correctamente'});
  });


}
refrescarListaRutina() {
  this.RutinasService.getrutina().subscribe((res) => {
    this.RutinasService.rutinas1 = res as Rutinas[];});
}

}