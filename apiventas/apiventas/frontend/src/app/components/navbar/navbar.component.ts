import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  navbarOpen = false;
  constructor(private _router: Router, public _service: ClienteService) { 
    
  }
  logoutUser() {
    debugger;
    localStorage.removeItem('token')
    localStorage.removeItem('cliente')
    localStorage.removeItem('tipo')
    localStorage.removeItem('id')
    this._router.navigate(['/login'])
  }
  private sidenav: MatSidenav;
  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
}

  ngOnInit(): void {
  }
  public close() {
    return this.sidenav.close();
}
toggleNavbar() {
  this.navbarOpen = !this.navbarOpen;
}


}
