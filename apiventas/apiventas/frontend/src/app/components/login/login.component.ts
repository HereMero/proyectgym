import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Clientes } from 'src/app/models/clientes';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  contra;
  nombreusu;
  submitted=false; 
  constructor(public ClienteService: ClienteService, private router: Router, private toastr: ToastrService) { }
 
  ngOnInit(){
    
  }
  onSubmit() {
    debugger;
    const user: Clientes = new Clientes();
     
    user.nombreusu = this.nombreusu;
    user.contra = this.contra;
    this.ClienteService.login(user)
      .subscribe((resp: any) => {
        this.router.navigate(['home']);
      
        let userSave = new Clientes();
        let UserId = new Clientes();
        let UserGuardado = new Clientes();
        UserId = resp._id
        userSave.tipo = resp.tipo;
        UserGuardado.nombreusu = resp.nombreusu;
        UserGuardado.nombre = resp.nombre;
        UserGuardado.direccion = resp.direccion;
        UserGuardado.edad = resp.edad;
        UserGuardado.telefono = resp.telefono;
        var userObj = JSON.stringify(UserId)
        var userObject = JSON.stringify(userSave);
        var UserObjeto = JSON.stringify(UserGuardado);
        localStorage.setItem('cliente', UserObjeto);
        localStorage.setItem('tipo', userObject);
        localStorage.setItem('id', userObj);
        debugger;
      }, error => {   this.toastr.warning('El nombre de usuario o contraseña es incorrecta.', 'Autenticacion Fallida.');
   
          
      
        }
      )
  }
}
