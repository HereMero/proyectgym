import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientestablaComponent } from './clientestabla.component';

describe('ClientestablaComponent', () => {
  let component: ClientestablaComponent;
  let fixture: ComponentFixture<ClientestablaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientestablaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientestablaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
