import { Component, OnInit } from '@angular/core';
import { Clientes } from 'src/app/models/clientes';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Toast, ToastrService } from 'ngx-toastr';

declare var M: any;

@Component({
  selector: 'app-clientestabla',
  templateUrl: './clientestabla.component.html',
  styleUrls: ['./clientestabla.component.css']
})

export class ClientestablaComponent implements OnInit {
  filterClien = "";
  constructor(public clienteService: ClienteService, public router: Router, private toastr: ToastrService) { }
 
  ngOnInit() {

    this.getClientes();

    this.resetForm();
  }
  getClientes() {
    this.clienteService.getcliente()
      .subscribe(res => {
        this.clienteService.clientes1= res as Clientes[];
      });
  }
  // editCliente(cliente: Clientes) {
  //   debugger;
  //   this.clienteService.selectclient = cliente;
  
  // }
  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.clienteService.selectclient = {
      _id: "",
      nombreusu: "",
      nombre:"",
      contra: "",
      direccion: "",
      telefono: "",
      edad: null,
      tipo:null

    }
  }
 
  refrescarListaCliente() {
    this.clienteService.getcliente().subscribe((res) => {
      this.clienteService.clientes1 = res as Clientes[];
    });
  }
  
  addcliente(form: NgForm)
  { 
    debugger;
    if (form.value._id == "" ) {
      this.clienteService.postcliente(form.value).subscribe((res) => {
        
        this.resetForm(form);
        
        debugger;
        this.refrescarListaCliente();
        M.toast({ html: 'Se guardo correctamente', classes: 'rounded' });
       return  error => Swal.fire({title: "No se puede crear dos veces el nombre del cliente modifiquelo"})
      });
    }
    else {
      this.clienteService.putcliente(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refrescarListaCliente();
        M.toast({ html: 'Se actualizo correctamente', classes: 'rounded' });
        error => Swal.fire({title: "No se pudo actualizar el usuario"})
      });

    }
    if(form.value.nombre == ""){ Swal.fire({title:"El nombre es requerido"})} 
    if(form.value.nombreusu == ""){ Swal.fire({title:"El nombre de usuario es requerido"})} 
    if(form.value.contra == ""){Swal.fire({title:"La contraseña es requerida"})}
    if(form.value.direccion == ""){Swal.fire({title:"La direccion es requerida"})}
    if(form.value.telefono == ""){Swal.fire({title:"El telefono es requerida"})}
    if(form.value.edad == null){Swal.fire({title:"La edad es requerida"})}
    if(form.value.tipo == null){Swal.fire({title:"El tipo es requerido"})}
   
}
 
  
  deleteUser(_id: string, form: NgForm ){
    debugger;
    Swal.fire({
      title: 'Esta seguro de eliminar este cliente?',
      text: "No hay vuelta atra!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.value) {
        this.clienteService.deletecliente(_id).subscribe((data) => {
      
          this.refrescarListaCliente();
            this.resetForm(form);
        
        });
        Swal.fire(
          'Se ha eliminado!',
          'Se elimino el cliente exitosamente.',
          'success'
        )
      }
    })
   
  
  
 }
 onEdit(cliente: Clientes) {
   debugger;
  this.clienteService.selectclient = cliente;
}
 get(_id: string) {
  this.clienteService.get(_id)
    .subscribe(res => {
      this.clienteService.clientes1 = res as Clientes[];
    });
}
  
  }
 



