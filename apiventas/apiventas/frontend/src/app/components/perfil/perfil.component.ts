import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
profile: any;
  constructor(_service: ClienteService) { 
    this.profile = JSON.parse(localStorage.getItem('cliente'))
  }

  ngOnInit(): void {
  }

}
