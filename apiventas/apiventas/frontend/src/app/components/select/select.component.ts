import { Component, OnInit } from '@angular/core';
import { Rutinas } from 'src/app/models/rutinas';
import { NgForm } from '@angular/forms';
import { RutinasService } from 'src/app/services/rutinas.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { Clientes } from 'src/app/models/clientes';
declare var M: any;
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

 id: any;
  constructor(public RutinasService: RutinasService, public ClienteService: ClienteService)
   { 
    this.id = JSON.parse(localStorage.getItem("id"))
   }

  ngOnInit(): void {
    this.getRutinas();
    this.resetForm();
    this.get(this.id);
    this.getRutinasid(this.id);
    this.getRutinasUni();
  }
  getRutinas() {
    debugger
    this.RutinasService.getrutina()
      .subscribe(res => {
        this.RutinasService.rutinas1= res as Rutinas[];
      });
  }
 
getRutinasid(_id: string)
{

  this.RutinasService.getrutina()
    .subscribe(res => {
      this.RutinasService.rutinas1= res as Rutinas[];
    });

}

getRutinasUni()
{
  this.RutinasService.getrutinauni().subscribe(res => {
    this.RutinasService.rutinas2= res as Rutinas[];});
}




addrutina(form: NgForm)
{
this.RutinasService.postrutina(form.value).subscribe(res =>{console.log(res)});
}

Guardar(form: NgForm)
{
  
this.RutinasService.postrutinauni(form.value).subscribe(res =>{console.log(res)});
}

get(_id: string) {
  debugger
  this.ClienteService.get(_id)
    .subscribe(res => {
      this.ClienteService.clientes1 = res as Clientes[];
    });

}



resetForm(form?: NgForm)
  {
   if(form)
   { 
    form.reset();
    this.RutinasService.selectrutina = new Rutinas();
   }
  }
  onEdit(rutina: Rutinas) {
    debugger;
   this.RutinasService.selectrutina = rutina;
 }
 deleteRutina(_id: string, form: NgForm ){
  debugger;
  this.RutinasService.deleterutinauni(_id).subscribe((data) => {
    
    this.refrescarListaRutina();
      this.resetForm(form);
  M.toast({html: 'Se elimino correctamente'});
  });


}
refrescarListaRutina() {
  this.RutinasService.getrutina().subscribe((res) => {
    this.RutinasService.rutinas1 = res as Rutinas[];});
}

}
