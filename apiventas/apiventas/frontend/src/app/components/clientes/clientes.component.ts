import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { Form, NgForm } from '@angular/forms';
import { Clientes } from 'src/app/models/clientes';
import { Router } from '@angular/router';
import { Toast, ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { Rutinas } from 'src/app/models/rutinas';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  providers: [ClienteService]
})
export class ClientesComponent implements OnInit {

  constructor(public ClienteService : ClienteService, private router: Router,  private toastr: ToastrService) {

   }

   form = {
     _id: '',
    nombre: '',
    nombreusu: '',
    contra: '',
    tipo: false
  };
  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.ClienteService.selectclient = {
      _id: "",
      nombreusu: "",
      nombre:"",
      contra: "",
      direccion: "",
      telefono: "",
      edad: null,
      tipo:false,

    }
  }

  addcliente(form: NgForm)
  { 
    debugger;
    this.ClienteService.postcliente(form.value).subscribe(res => {
    console.log(res)
    
    
    this.toastr.success('Se creo el usuario correctamente, inicia sesión')
    error => Swal.fire({title: "No se puedo crear el usuario"})
        this.router.navigate(['/login']);
      
  });

 }
    
  }



