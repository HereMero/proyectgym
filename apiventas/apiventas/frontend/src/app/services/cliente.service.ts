import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Clientes } from '../models/clientes';
import { isNullOrUndefined } from 'util';



@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  selectclient: Clientes;
  clientes1: Clientes[];
  
  readonly url_api ='http://localhost:4000/clientes';
  readonly url_apilog ='http://localhost:4000/login';
  constructor(private http: HttpClient) 
  {
   this.selectclient = new Clientes();
  
  }
  
  getcliente()
  {
      return this.http.get('http://localhost:4000/clientes');
  }
  get(_id:string)
  {
    return this.http.get(`${this.url_api}`+ `/${_id}`);
  }
   
  
  postcliente(clientes: Clientes){
   
    
    return this.http.post(`${this.url_api}`, clientes);
  }

 
  postRutinaclient(clientes: Clientes)
  {
    return this.http.post(`${this.url_api}`, clientes)
  }



  putcliente(clientes: Clientes)
  { 
   
  return this.http.put(`${this.url_api}`, clientes);
  }

  deletecliente(_id: string)
  {
    return this.http.delete(this.url_api+ `/${_id}`);
  }
  login(user) {
    return this.http.post<any>(`${this.url_apilog}` , user);
  }
   loggedIn() {
    return !!localStorage.getItem('cliente');
    
    
  }
  IsAdmin(){
    debugger;
    let Thisuser = localStorage.getItem("tipo");
    if (!isNullOrUndefined(Thisuser)) {
      var user = Thisuser;
      if (user == '{"tipo":true}') {
        return true;
      } else {
        if(user== '{"tipo":false}')
        {
          return false
        }
     
      }
    
  }
}
  
  getToken() {
    return localStorage.getItem('token');
  }



}
