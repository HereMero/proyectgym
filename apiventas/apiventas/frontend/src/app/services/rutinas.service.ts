import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Rutinas} from '../models/rutinas';

@Injectable({
  providedIn: 'root'
})
export class RutinasService {
  
  selectrutina: Rutinas;
   rutinas1: Rutinas[];
   rutinas2: Rutinas[];
   id: any;
   readonly url_api ='http://localhost:4000/rutinas'
   readonly url_api2 ='http://localhost:4000/rutinasuni'
  constructor(private http: HttpClient) 
  {
    this.selectrutina = new Rutinas();
    this.id = JSON.parse(localStorage.getItem("id"))

  }

 getrutina()
 {
   return this.http.get('http://localhost:4000/rutinas');
 }

 get(_id:string)
 {
     return this.http.get(`${this.url_api}`+ `/${_id}`);
 }
 getuni(_id:string)
 {
     return this.http.get(`${this.url_api}`+ `/${_id}`);
 }



 postrutina(rutinas: Rutinas)
 {
   return this.http.post(`${this.url_api}`, rutinas);
 }

putrutina(rutinas: Rutinas)
{
  return this.http.put(`${this.url_api}`, rutinas);
}

deleterutina(_id:string)
{
  return this.http.delete(this.url_api+ `/${_id}`);
}
getrutinauni()
{
  return this.http.get('http://localhost:4000/rutinasuni');

}

postrutinauni(rutinas: Rutinas)
{
 
   rutinas.usuarioID = this.id
  return this.http.post(`${this.url_api2}`, rutinas);
}
deleterutinauni(_id:string)
{
  return this.http.delete(this.url_api2+ `/${_id}`);
}
}
