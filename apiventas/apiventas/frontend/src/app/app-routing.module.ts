import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import { ErrorComponent } from './components/error/error.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { HomeComponent } from './components/home/home.component';
import { ClientestablaComponent } from './components/clientestabla/clientestabla.component';
import { RutinasComponent } from './components/rutinas/rutinas.component';
import { AuthGuardComponent } from './auth-guard/auth-guard.component';
import { AuthAdmin } from './auth-guard/authadmin.guard';
import { PerfilComponent } from './components/perfil/perfil.component';
import { SelectComponent } from './components/select/select.component';

const routes: Routes = [
  { path: 'home', component:HomeComponent},
  {path:'clientes', component:ClientesComponent},
  {path: 'login', component:LoginComponent},
  {path: 'perfil', component:PerfilComponent, canActivate:[AuthGuardComponent]},
  {path:'clientestabla', component:ClientestablaComponent, canActivate:[ AuthAdmin]},
  {path: 'rutinas', component: RutinasComponent, canActivate:[ AuthAdmin]},
  {path: '***', component: ErrorComponent},
  {path: '', component: LoginComponent},
  {path: 'select', component: SelectComponent, canActivate:[AuthGuardComponent]}

  
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
