const config = {
    application: {
        cors: {
            server: [
                {
                    origin: "http://localhost:4000", //servidor que deseas que consuma o (*) en caso que sea acceso libre
                    credentials: true
                }
            ]
        }
}
}
module.exports = config;