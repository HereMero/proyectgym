const mongoose=require("mongoose");
const Schema=mongoose.Schema;
const ClientesSchema= require("../schemas/Clientes.schema");
const clientes = new ClientesSchema();
const RutinasSchema= new Schema({
    nombre:{type:String, required:true},
    descripcion:{type:String, required:true},
}, { versionKey: false });

module.exports = mongoose.model("rutinas",RutinasSchema,"rutinas");