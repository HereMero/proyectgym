const mongoose=require("mongoose");
const Schema=mongoose.Schema;
const RutinasUniSchema= new Schema({
    usuarioID:{type:String, required:true},
    nombre:{type:String, required:true},
    descripcion:{type:String, required:true},
}, { versionKey: false });

module.exports = mongoose.model("rutinasuni",RutinasUniSchema,"rutinasuni");