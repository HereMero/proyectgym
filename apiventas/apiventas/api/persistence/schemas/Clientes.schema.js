const mongoose=require("mongoose");
const Schema=mongoose.Schema;

const ClientesSchema= new Schema({
    nombreusu:{type:String, required:true},
    nombre:{type:String, required:true},
    contra:{type:String, required:true},
    direccion:{type:String, required:true},
    telefono:{type:String, required:true},
    edad:{type:Number, required:true, min:0},
    tipo:{type:Boolean, required:true},
}, { versionKey: false });

module.exports = mongoose.model("clientes",ClientesSchema,"clientes");