const RutinasSchema= require("../schemas/Rutinas.schema");
const mongoose = require('mongoose');
class RutinasRepository{
    async findAll(){
        let rutinas=await RutinasSchema.find();
        return rutinas;
    }

    async findById(idRutina){
        let rutina = await RutinasSchema.findOne({_id: idRutina});
        return rutina;
    }

    async create(rutina){
        rutina = new RutinasSchema(rutina);
        let rutinaGuardado= await rutina.save();
        return rutinaGuardado;
    }
    async delete (idRutina){
        let rutina= await this.findById(idRutina);
        if(rutina){
            await rutina.remove();
        }else{
            throw Error(`Producto ${idRutina} no encontrado`);
        }
    }

    async update(rutinaEditado){
        let rutina= await this.findById(rutinaEditado._id);
        if(rutina){
             rutina.nombre = rutinaEditado.nombre;
             rutina.descripcion = rutinaEditado.descripcion;
            return await rutina.save();
        }else{
            throw Error(`Producto ${rutinaEditado._id} no encontrado`);
        }
    }
}

module.exports = RutinasRepository;