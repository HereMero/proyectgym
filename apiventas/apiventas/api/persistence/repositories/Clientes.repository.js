const ClientesSchema= require("../schemas/Clientes.schema");
const mongoose = require('mongoose');
class ClientesRepository{
    async findAll(){
        let clientes=await ClientesSchema.find();
        return clientes;
    }

    async findById(idCliente){
        let cliente = await ClientesSchema.findOne({_id: idCliente});
        return cliente;
    }

    async create(cliente){
        cliente = new ClientesSchema(cliente);
        let clienteGuardado= await cliente.save();
        return clienteGuardado;
    }

    async delete (idCliente){
        let cliente= await this.findById(idCliente);
        if(cliente){
            await cliente.remove();
        }else{
            throw Error(`Producto ${idCliente} no encontrado`);
        }
    }

    async update(clienteEditado){
        let cliente= await this.findById(clienteEditado._id);
        if(cliente){
            cliente.nombre=clienteEditado.nombre;
            cliente.contra= clienteEditado.contra;
            cliente.direccion=clienteEditado.direccion;
            cliente.telefono=clienteEditado.telefono;
            cliente.edad=clienteEditado.edad;
            cliente.tipo = clienteEditado.tipo;
            return await cliente.save();
        }else{
            throw Error(`Producto ${clienteEditado._id} no encontrado`);
        }
    }
}
ClientesSchema.statics = {
   
    login: function (query, cb) {
      this.find(query, cb);
    }
  }
//const ClientesSchema = mongoose.model('clientes', authSchema);

module.exports = ClientesRepository;