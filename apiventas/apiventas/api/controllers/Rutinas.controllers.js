const RutinasRepository = require("../persistence/repositories/Rutinas.repository");
const rutinasRepo= new RutinasRepository();

class RutinasController{

    async findAll(req,res){
        try{
            let Rutinas=await rutinasRepo.findAll();
            res.status(200).json(Rutinas);
        }
        catch (error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
    async findById(req,res){
        let idRutina= req.params.idRutina;
        let rutina = await rutinasRepo.findById(idRutina);
 
        if(rutina)
            res.send(rutina);
        else
            res.status(404).send(`Cliente ${idRutina} no encontrado`);
    }
     async create(req,res){
        debugger
        try{
        let rutina=req.body;
        if(rutina._id || rutina._id===""){
            delete rutina._id;
        }
        let rutinaGuardado=await rutinasRepo.create(rutina);
        res.send(rutinaGuardado);
        }
        catch (error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
    async update(req,res){
        let rutinaEnviado=req.body;
        try{
            let rutinaGuardado=await rutinasRepo.update(rutinaEnviado)
            res.status(200).json(rutinaGuardado);
        }catch(error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
    async delete(req,res){
        try{
            let idRutina= req.params.idRutina;
            await rutinasRepo.delete(idRutina);
            res.status(200).json(`Rutina ${idRutina} eliminada`);
        }
    catch(error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }

}

module.exports = RutinasController;
