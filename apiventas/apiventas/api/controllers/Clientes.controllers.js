const ClientesRepository = require("../persistence/repositories/Clientes.repository");
const clientesRepo= new ClientesRepository();

class ClientesController{

    async findAll(req,res){
        try{
            let Clientes=await clientesRepo.findAll();
            res.status(200).json(Clientes);
        }
        catch (error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
    async findById(req,res){
        let idCliente= req.params.idCliente;
        let cliente = await clientesRepo.findById(idCliente);
 
        if(cliente)
            res.send(cliente);
        else
            res.status(404).send(`Cliente ${idCliente} no encontrado`);
    }
     async create(req,res){
        debugger
        try{
        let cliente=req.body;
        if(cliente._id || cliente._id===""){
            delete cliente._id;
        }
        let clienteGuardado=await clientesRepo.create(cliente);
        res.send(clienteGuardado);
        }
        catch (error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
   
    async update(req,res){
        let clienteEnviado=req.body;
        try{
            let clienteGuardado=await clientesRepo.update(clienteEnviado)
            res.status(200).json(clienteGuardado);
        }catch(error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }
    async delete(req,res){
        try{
            let idCliente= req.params.idCliente;
            await clientesRepo.delete(idCliente);
            res.status(200).json(`Producto ${idCliente} eliminado`);
        }
    catch(error){
            console.error(error);
            res.status(500).json(error.message);
        }
    }

}

module.exports = ClientesController;
