const express= require("express");
const router = express.Router();
const jwt = require('jsonwebtoken');
const Cliente = require("../api/persistence/schemas/Clientes.schema")
const ClientesController=require("../api/controllers/Clientes.controllers");
const clientesController= new ClientesController();
const Rutinas = require("../api/persistence/schemas/Rutinas.schema");
const RutinasController = require("../api/controllers/Rutinas.controllers");
const rutinasController = new RutinasController();
const RutinasUniController = require("../api/controllers/RutinasUni.controllers");
const rutinasuniController = new RutinasUniController();

router.post('/login', async (req, res) => {
        const { nombreusu, contra } = req.body;
    
        const user = await Cliente.findOne({nombreusu});
        if (!user) return res.status(401).send('El nombre no \' existe');
        if (user.contra !== contra) return res.status(401).send('contraseña incorrecta');
    
					const token = jwt.sign({
						_id: user._id,
						nombreusu: user.nombreusu,
						nombre: user.nombre,
						tipo: user.tipo
					}, 'secretkey');
					res.send(user);
		return res.status(200).json({token});
		
    });
    
      
/************************* CLIENTES **********************/
router.get("/clientes", clientesController.findAll);  
router.get("/clientes/:idCliente",clientesController.findById);
router.post("/clientes",clientesController.create);
router.delete("/clientes/:idCliente",clientesController.delete);
router.put("/clientes",clientesController.update);
/************************* RUTINAS **********************/
router.get("/rutinas",rutinasController.findAll);  
router.get("/rutinas/:idRutina",rutinasController.findById);
router.post("/rutinas",rutinasController.create);
router.delete("/rutinas/:idRutina",rutinasController.delete);
router.put("/rutinas",rutinasController.update);
/************************* RUTINASUNI **********************/
router.get("/rutinasuni",rutinasuniController.findAll);  
router.get("/rutinasuni/:idRutina",rutinasuniController.findById);
router.post("/rutinasuni",rutinasuniController.create);
router.delete("/rutinasuni/:idRutina",rutinasuniController.delete);
router.put("/rutinasuni",rutinasuniController.update);


router.get("/",(req,res )=>{
        res.send("Hola,Sean Todos Bienvenidos Hijos del Rock and Roll !!!!... los saludan los aliados de la noche  en express");
});

    

module.exports = router;