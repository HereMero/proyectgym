const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const requestconfigs=require("./requestConfigs");
const router= require("./router/router");
const {Mongoose} =require('./dbconnection');
const app = express();
const config = require('./config');
//configuracion
app.set('port', process.env.PORT || 4000);

//middlewares
app.use(morgan('dev'))
app.use(express.json());
app.use(requestconfigs.setHeaders);
app.use(cors(config.application.cors.server));

const bodyParser = require('body-parser');
const bodyParserJSON = bodyParser.json();
const bodyParserURLEncoded = bodyParser.urlencoded({ extended: true });

app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);
app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);

app.use("/",router);




//server empesando
app.listen(app.get('port'), () =>{
    console.log('Server en el puerto', app.get('port'));
})